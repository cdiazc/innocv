package com.cdiaz.crudinnocv.presenters;

import com.cdiaz.crudinnocv.dataretrieval.DataRepository;
import com.cdiaz.crudinnocv.model.Person;
import com.cdiaz.crudinnocv.presenters.contracts.MainActivityPresenterContract;
import com.cdiaz.crudinnocv.views.contracts.MainActivityContract;

import java.util.List;

/**
 * Created by carlos on 24/02/2018.
 */

public class MainActivityPresenter implements MainActivityPresenterContract {

    private MainActivityContract view;
    private DataRepository repository;

    public MainActivityPresenter(final MainActivityContract view, final DataRepository repository) {
        this.view = view;
        this.repository = repository;
    }

    @Override
    public void getAllData() {
        view.showLoadingScreen();
        repository.getAllData(this);
    }

    @Override
    public void openUpdate(final Person p) {
        view.openEditAddPopup(p);
    }

    @Override
    public void update(final Person p) {
        repository.update(this, p);
    }


    @Override
    public void create(final Person p) {
        repository.create(this, p);
    }

    @Override
    public void delete(final int id) {
        view.closeFilter();
        repository.delete(this, id);
    }


    @Override
    public void onDataReceived(final Object o) {
        view.onDataReceived((List<Person>) o);
    }

    @Override
    public void onDeletedSuccess(final int id) {
        view.onDeletedSuccess(id);
    }

    @Override
    public void onUpdatedSuccess(final Person p) {
        view.onUpdatedSuccess(p);
    }

    @Override
    public void onCreatedSuccess(final Person p) {
        view.onCreatedSuccess(p);
    }

    @Override
    public void onError() {
        view.onError();
    }
}
