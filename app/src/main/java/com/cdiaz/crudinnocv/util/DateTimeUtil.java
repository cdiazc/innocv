package com.cdiaz.crudinnocv.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by carlos on 25/02/2018.
 */

public class DateTimeUtil {


    public static String toInnoCvDateFormat(final long timestamp) {

        final Calendar pickedCalendar = Calendar.getInstance();
        pickedCalendar.setTimeInMillis(timestamp);

        return pickedCalendar.get(Calendar.YEAR) + "-" + (pickedCalendar.get(Calendar.MONTH)+1) + "-" + pickedCalendar.get(Calendar.DAY_OF_MONTH) + "T00:00:00";

    }

    public static long fromInnoCvDateFormat(final String dateString) {
        final SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        long ret = 0;

        try {
            final Date date = parser.parse(dateString);
            ret = date.getTime();
        } catch (final ParseException e) {
            e.printStackTrace();
        }

        return ret;

    }
}
