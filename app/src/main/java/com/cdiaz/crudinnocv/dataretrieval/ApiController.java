package com.cdiaz.crudinnocv.dataretrieval;

import com.cdiaz.crudinnocv.model.Person;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by carlos on 24/02/2018.
 */

public interface ApiController {

    @GET("getall")
    Call<List<Person>> getAll();

    @GET("get/{id}")
    Call<Person> get(@Path("id") final int id);

    @POST("update")
    Call<Person> update(@Body Person person);

    @POST("create")
    Call<Person> create(@Body Person person);

    @GET("remove/{id}")
    Call<Void> remove(@Path("id") final int id);

}
