package com.cdiaz.crudinnocv.presenters.contracts;

import com.cdiaz.crudinnocv.model.Person;

import java.util.List;

/**
 * Created by carlos on 24/02/2018.
 */

public interface GenericPresenterContract {

    public void onDataReceived(final Object o);
    public void onDeletedSuccess(final int id);
    public void onUpdatedSuccess(final Person p);
    public void onCreatedSuccess(final Person p);

    public void onError();
}
