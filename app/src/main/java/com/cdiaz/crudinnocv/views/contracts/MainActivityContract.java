package com.cdiaz.crudinnocv.views.contracts;

import com.cdiaz.crudinnocv.model.Person;

import java.util.List;

/**
 * Created by carlos on 24/02/2018.
 */

public interface MainActivityContract {

    public void onDataReceived(final List<Person> personList);
    public void onDeletedSuccess(final int id);
    public void onUpdatedSuccess(final Person p);
    public void onCreatedSuccess(final Person p);

    public void onError();

    public void showLoadingScreen();
    public void hideLoadingScreen();

    public void showErrorScreen();
    public void hideErrorScreen();

    public void openEditAddPopup(final Person p);

    public void closeFilter();

    }
