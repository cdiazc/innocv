package com.cdiaz.crudinnocv.adapters;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.cdiaz.crudinnocv.R;
import com.cdiaz.crudinnocv.databinding.DataElementBinding;
import com.cdiaz.crudinnocv.model.Person;
import com.cdiaz.crudinnocv.presenters.MainActivityPresenter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by carlos on 02/01/2018.
 */

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.DataViewHolder> {

    private List<Person> personList;
    private List<Person> completeList;

    DataElementBinding bindings;
    MainActivityPresenter presenter;


    public DataAdapter(final List<Person> personList, final MainActivityPresenter presenter) {
        this.personList = personList;
        this.presenter = presenter;
    }


    public void addElement(final Person personToAdd) {
        personList.add(personToAdd);
    }

    public void updateElement(final Person personToEdit) {
        for (final Person p : personList) {
            if (p.equals(personToEdit)) {
                personList.set(personList.indexOf(p), personToEdit);
            }
        }
    }

    public void deleteWithId(final int id) {
        for (final Person p : personList) {
            if (p.id == id) {
                personList.remove(p);
                break;
            }
        }
    }

    public void initFilter() {
        completeList = personList;
        notifyDataSetChanged();
    }

    public void stopFilter() {
        personList = completeList;
        completeList = null;
        notifyDataSetChanged();
    }

    public void filter(final String s) {

        personList = new ArrayList<>();

        for (final Person p : completeList) {
            if (p.name.toLowerCase().contains(s.toLowerCase())) {
                personList.add(p);
            }
        }

        notifyDataSetChanged();
    }


    @Override
    public DataViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        final LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        bindings = DataBindingUtil.inflate(layoutInflater, R.layout.data_element, parent, false);
        bindings.setPresenter(presenter);
        return new DataViewHolder(bindings);
    }

    @Override
    public void onBindViewHolder(final DataViewHolder holder, final int position) {
        final Person person = personList.get(position);
        holder.bind(person);
    }

    @Override
    public int getItemCount() {
        return personList.size();
    }

    public void setData(final List<Person> personList) {
        this.personList = personList;
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        DataElementBinding bindings;

        public void bind(final Person person) {
            bindings.setPerson(person);
            bindings.executePendingBindings();
        }

        public DataViewHolder(final DataElementBinding bindings) {
            super(bindings.getRoot());
            this.bindings = bindings;
        }
    }

}
