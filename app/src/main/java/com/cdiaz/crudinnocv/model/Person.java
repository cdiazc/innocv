package com.cdiaz.crudinnocv.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by carlos on 24/02/2018.
 */

public class Person {

    @SerializedName("id")
    public int id;

    @SerializedName("name")
    public String name;

    //Comes as a String from the server, better be as
    // purist as possible to prevent regression.
    @SerializedName("birthdate")
    public String birthdate;

    @Override
    public boolean equals(final Object obj) {

        if (obj instanceof Person && ((Person) obj).id == id) {
            return true;
        }

        return false;
    }
}
