package com.cdiaz.crudinnocv.bindings;

import android.databinding.BindingAdapter;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by carlos on 24/02/2018.
 */

public class BindingAdapters {

    @BindingAdapter("birthdate")
    public static void setBirthdate(final TextView textView, final String birthdateString) {
        final SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        final SimpleDateFormat result = new SimpleDateFormat("dd/MM/yyyy");
        String birthdate = birthdateString;

        try {
            final Date d = parser.parse(birthdateString);
            birthdate = result.format(d);
        } catch (final ParseException e) {
            e.printStackTrace();
        } finally {
            textView.setText(birthdate);
        }
    }
}
