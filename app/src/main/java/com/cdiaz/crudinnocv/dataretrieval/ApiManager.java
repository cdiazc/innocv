package com.cdiaz.crudinnocv.dataretrieval;

import com.cdiaz.crudinnocv.model.Person;
import com.cdiaz.crudinnocv.presenters.contracts.GenericPresenterContract;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by carlos on 24/02/2018.
 */

public class ApiManager {

    private final Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://hello-world.innocv.com/api/user/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    private final ApiController api = retrofit.create(ApiController.class);

    public void getAllData(final GenericPresenterContract presenter) {

        final Call<List<Person>> call = api.getAll();

        call.enqueue(new Callback<List<Person>>() {
            @Override
            public void onResponse(final Call<List<Person>> call, final Response<List<Person>> response) {

                if (response.code() < 400) {
                    presenter.onDataReceived(response.body());
                }
                else {
                    presenter.onError();
                }
            }

            @Override
            public void onFailure(final Call<List<Person>> call, final Throwable t) {
                presenter.onError();
            }
        });

    }

    public void update(final GenericPresenterContract presenter, final Person person) {

        final Call<Person> call = api.update(person);

        call.enqueue(new Callback<Person>() {
            @Override
            public void onResponse(Call<Person> call, Response<Person> response) {
                if (response.code() < 400) {
                    presenter.onUpdatedSuccess(response.body());
                }
                else {
                    presenter.onError();
                }
            }

            @Override
            public void onFailure(Call<Person> call, Throwable t) {
                presenter.onError();
            }
        });
    }


    public void create(final GenericPresenterContract presenter, final Person person) {

        final Call<Person> call = api.create(person);

        call.enqueue(new Callback<Person>() {
            @Override
            public void onResponse(Call<Person> call, Response<Person> response) {
                if (response.code() < 400) {
                    presenter.onCreatedSuccess(response.body());
                }
                else {
                    presenter.onError();
                }
            }

            @Override
            public void onFailure(Call<Person> call, Throwable t) {
                presenter.onError();
            }
        });
    }


    public void delete(final GenericPresenterContract presenter, final int id) {

        final Call<Void> call = api.remove(id);

        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {

                if (response.code() < 400) {
                    presenter.onDeletedSuccess(id);
                }
                else {
                    presenter.onError();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                presenter.onError();
            }
        });

    }


}
