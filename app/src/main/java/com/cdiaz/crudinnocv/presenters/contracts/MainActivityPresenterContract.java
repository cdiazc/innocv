package com.cdiaz.crudinnocv.presenters.contracts;

import com.cdiaz.crudinnocv.model.Person;

/**
 * Created by carlos on 24/02/2018.
 */

public interface MainActivityPresenterContract extends GenericPresenterContract {

    public void getAllData();
    public void openUpdate(final Person p);
    public void update(final Person p);
    public void create(final Person p);
    public void delete(final int it);
}
