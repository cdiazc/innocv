package com.cdiaz.crudinnocv.views;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.cdiaz.crudinnocv.R;
import com.cdiaz.crudinnocv.adapters.DataAdapter;
import com.cdiaz.crudinnocv.databinding.ActivityMainBinding;
import com.cdiaz.crudinnocv.dataretrieval.DataRepository;
import com.cdiaz.crudinnocv.model.Person;
import com.cdiaz.crudinnocv.presenters.MainActivityPresenter;
import com.cdiaz.crudinnocv.util.DateTimeUtil;
import com.cdiaz.crudinnocv.views.contracts.MainActivityContract;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements MainActivityContract {

    private boolean isSearchBoxOpen;
    private EditText filterEdittext;
    private MenuItem menuItemSearch;


    private ActivityMainBinding bindings;

    private RecyclerView recyclerView;
    private DataAdapter adapter;

    private MainActivityPresenter presenter;

    private DataRepository repository;


    ///////////////////////////////////
    ///////////LIFECYCLE///////////////
    ///////////////////////////////////
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        bindings = DataBindingUtil.setContentView(this, R.layout.activity_main);

        repository = new DataRepository();

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openEditAddPopup(null);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        presenter = new MainActivityPresenter(this, repository);
        bindings.setPresenter(presenter);

        recyclerView = bindings.activityMainDataContainer;

        adapter = new DataAdapter(new ArrayList<Person>(), presenter);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(MainActivity.this.getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        presenter.getAllData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        bindings     = null;
        recyclerView = null;
        adapter      = null;
        presenter    = null;
        repository   = null;

    }

    ///////////////////////////////////





    ///////////////////////////////////
    ///////////OPTIONS/////////////////
    ///////////////////////////////////
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        menuItemSearch = menu.findItem(R.id.action_search);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_search) {
            handleMenuSearch(false);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    ///////////////////////////////////





    ///////////////////////////////////
    //////////////UI///////////////////
    ///////////////////////////////////
    @Override
    public void onDataReceived(final List<Person> personList) {
        hideLoadingScreen();
        adapter.setData(personList);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onDeletedSuccess(final int id) {
        Snackbar.make(bindings.getRoot(), getString(R.string.activity_main_deleted), Snackbar.LENGTH_LONG).show();
        adapter.deleteWithId(id);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onUpdatedSuccess(final Person p) {
        Snackbar.make(bindings.getRoot(), getString(R.string.activity_main_updated), Snackbar.LENGTH_LONG).show();
        adapter.updateElement(p);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onCreatedSuccess(final Person p) {
        Snackbar.make(bindings.getRoot(), getString(R.string.activity_main_created), Snackbar.LENGTH_LONG).show();
        adapter.addElement(p);
        adapter.notifyDataSetChanged();

    }

    @Override
    public void onError() {
        showErrorScreen();
    }

    @Override
    public void showLoadingScreen() {
        hideErrorScreen();
        bindings.activityMainLoadingContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoadingScreen() {
        bindings.activityMainLoadingContainer.setVisibility(View.GONE);
    }

    @Override
    public void showErrorScreen() {
        hideLoadingScreen();
        bindings.activityMainErrorContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideErrorScreen() {
        bindings.activityMainErrorContainer.setVisibility(View.GONE);
    }

    @Override
    public void openEditAddPopup(final Person person) {

        closeFilter();

        final FragmentManager fm = getSupportFragmentManager();
        EditAddDialogFragment dialogFragment;

        if (person != null) {
            dialogFragment = EditAddDialogFragment.newInstance(false, person.name, person.id, DateTimeUtil.fromInnoCvDateFormat(person.birthdate));
        }
        else {
             dialogFragment = new EditAddDialogFragment();
        }

        dialogFragment.show(fm, "MainActivityPopup");

    }

    @Override
    public void closeFilter() {
        handleMenuSearch(true);
    }


    private void handleMenuSearch(final boolean forceClose) {

        final ActionBar actionbar = getSupportActionBar();

        if(isSearchBoxOpen){

            menuItemSearch.setIcon(getResources().getDrawable(R.drawable.ic_filter));

            actionbar.setDisplayShowCustomEnabled(false);
            actionbar.setDisplayShowTitleEnabled(true);

            //Hide keyboard
            final InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(filterEdittext.getWindowToken(), 0);

            isSearchBoxOpen = false;

            adapter.stopFilter();

        } else if (!forceClose) {

            adapter.initFilter();

            actionbar.setDisplayShowCustomEnabled(true);
            actionbar.setCustomView(R.layout.search_bar);
            actionbar.setDisplayShowTitleEnabled(false);

            filterEdittext = (EditText)actionbar.getCustomView().findViewById(R.id.search_bar_edittext);

            filterEdittext.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(final CharSequence charSequence, int i, int i1, int i2) {}

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    adapter.filter(charSequence.toString());
                }

                @Override
                public void afterTextChanged(Editable editable) {}
            });

            filterEdittext.requestFocus();

            //Open keyboard
            final InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(filterEdittext, InputMethodManager.SHOW_IMPLICIT);

            //Add the close icon
            menuItemSearch.setIcon(getResources().getDrawable(R.drawable.ic_close));

            isSearchBoxOpen = true;
        }
    }

    ///////////////////////////////////


    ///////////////////////////////////
    ///////////FRAGMENT COM////////////
    ///////////////////////////////////
    public void update(final Person p) {
        presenter.update(p);
    }

    public void create(final Person p) {
        presenter.create(p);
    }
    ///////////////////////////////////
}
