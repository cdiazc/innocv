package com.cdiaz.crudinnocv.views;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.cdiaz.crudinnocv.R;
import com.cdiaz.crudinnocv.dataretrieval.DataRepository;
import com.cdiaz.crudinnocv.model.Person;
import com.cdiaz.crudinnocv.util.DateTimeUtil;
import com.cdiaz.crudinnocv.views.contracts.MainActivityContract;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by carlos on 25/02/2018.
 */

public class EditAddDialogFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    private MainActivity activity;

    private static final String SELECTED_DATE_BUNDLE = "EditAddDialogFragment_SELECTED_DATE_BUNDLE";
    private static final long   SELECTED_DATE_DEFAULT = 0;

    private long pickedDate = SELECTED_DATE_DEFAULT;

    private TextView dateText;
    private EditText nameEditText;

    public static final String IS_NEW_BUNDLE = "EditAddDialogFragment_IS_NEW_BUNDLE";
    private boolean isNew = true;

    public static final String NAME_TO_EDIT_BUNDLE  = "EditAddDialogFragment_NAME_TO_EDIT_BUNDLE";
    public static final String ID_TO_EDIT_BUNDLE    = "EditAddDialogFragment_ID_TO_EDIT_BUNDLE";
    public static final String DATE_TO_EDIT_BUNDLE  = "EditAddDialogFragment_DATE_TO_EDIT_BUNDLE";
    private String nameToEdit;
    private int idToEdit;
    private long dateToEdit;


    public static EditAddDialogFragment newInstance(final boolean isNew, final String nameToEdit, final int idToEdit, final long dateToEdit) {

        final EditAddDialogFragment dialog = new EditAddDialogFragment();

        final Bundle bundle = new Bundle();
        bundle.putBoolean(IS_NEW_BUNDLE, isNew);
        bundle.putString(NAME_TO_EDIT_BUNDLE, nameToEdit);
        bundle.putInt(ID_TO_EDIT_BUNDLE, idToEdit);
        bundle.putLong(DATE_TO_EDIT_BUNDLE, dateToEdit);

        dialog.setArguments(bundle);

        return dialog;
    }



    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.edit_add_layout, container, false);

        activity = (MainActivity)getActivity();

        if (getArguments() != null) {
            this.isNew = getArguments().getBoolean(IS_NEW_BUNDLE, true);
            this.nameToEdit = getArguments().getString(NAME_TO_EDIT_BUNDLE, "");
            this.idToEdit = getArguments().getInt(ID_TO_EDIT_BUNDLE, -1);
            this.dateToEdit = getArguments().getLong(DATE_TO_EDIT_BUNDLE, SELECTED_DATE_DEFAULT);
        }

        dateText        = (TextView) rootView.findViewById(R.id.edit_add_layout_date_text);
        nameEditText    = (EditText) rootView.findViewById(R.id.edit_add_layout_name);

        if (savedInstanceState != null) {
            pickedDate = savedInstanceState.getLong(SELECTED_DATE_BUNDLE, SELECTED_DATE_DEFAULT);
        }
        else if (!isNew){
            pickedDate = this.dateToEdit;
        }

        if (pickedDate == SELECTED_DATE_DEFAULT) {
            pickedDate = new Date().getTime();
        }

        setDateText();


        if (!isNew) {
            setNameEditText(nameToEdit);
        }


        rootView.findViewById(R.id.edit_add_layout_date_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Calendar now = Calendar.getInstance();
                final DatePickerDialog dpd = DatePickerDialog.newInstance(
                        EditAddDialogFragment.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.show(EditAddDialogFragment.this.getActivity().getFragmentManager(), "Datepickerdialog");
            }
        });

        rootView.findViewById(R.id.edit_add_layout_go_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                if (!nameEditText.getText().toString().isEmpty()) {

                    final Person person = new Person();
                    person.name = nameEditText.getText().toString();
                    person.birthdate = DateTimeUtil.toInnoCvDateFormat(pickedDate);

                    if (isNew) {
                        activity.create(person);
                    }
                    else {
                        person.id = idToEdit;
                        activity.update(person);
                    }

                    EditAddDialogFragment.this.dismiss();

                }

            }
        });

        return rootView;
    }

    @Override
    public void onSaveInstanceState(final Bundle outState) {
        outState.putLong(SELECTED_DATE_BUNDLE, pickedDate);
        super.onSaveInstanceState(outState);
    }



    @Override
    public void onDateSet(final DatePickerDialog view, final int year, final int monthOfYear, final int dayOfMonth) {

        final Date date = new GregorianCalendar(year, monthOfYear, dayOfMonth).getTime();
        pickedDate = date.getTime();

        setDateText();

    }

    private void setNameEditText(final String text) {
        nameEditText.setText(text);
    }


    private void setDateText() {

        final Calendar pickedCalendar = Calendar.getInstance();
        pickedCalendar.setTimeInMillis(pickedDate);
        dateText.setText(pickedCalendar.get(Calendar.DAY_OF_MONTH) + "/" + (pickedCalendar.get(Calendar.MONTH)+1) + "/" + pickedCalendar.get(Calendar.YEAR));
    }
}
