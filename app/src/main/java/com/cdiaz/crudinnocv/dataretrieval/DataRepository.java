package com.cdiaz.crudinnocv.dataretrieval;

import android.content.Context;

import com.cdiaz.crudinnocv.model.Person;
import com.cdiaz.crudinnocv.presenters.contracts.GenericPresenterContract;

/**
 * Created by carlos on 24/02/2018.
 */

public class DataRepository {

    private ApiManager apiManager;

    public DataRepository() {
        apiManager = new ApiManager();
    }

    public void getAllData(final GenericPresenterContract presenter) {
        apiManager.getAllData(presenter);
    }

    public void update(final GenericPresenterContract presenter, final Person p) {
        apiManager.update(presenter, p);
    }

    public void create(final GenericPresenterContract presenter, final Person p) {
        apiManager.create(presenter, p);
    }

    public void delete(final GenericPresenterContract presenter, final int id) {
        apiManager.delete(presenter, id);
    }
}
